import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  TextStyle style = TextStyle(fontSize: 20.0);

  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Oeschinen Lake Campground',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'Kandersteg, Switzerland',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          FavoriteWidget(),
        ],
      ),
    );

    Color color = Theme.of(context).primaryColor;

    Column _buildButtonColumn(Color color, IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: color,
              ),
            ),
          ),
        ],
      );
    }

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.call, 'CALL'),
          _buildButtonColumn(color, Icons.near_me, 'ROUTE'),
          _buildButtonColumn(color, Icons.share, 'SHARE'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'Lake Oeschinen lies at the foot of the Blüemlisalp in the Bernese '
        'Alps. Situated 1,578 meters above sea level, it is one of the '
        'larger Alpine Lakes. A gondola ride from Kandersteg, followed by a '
        'half-hour walk through pastures and pine forest, leads you to the '
        'lake, which warms to 20 degrees Celsius in the summer. Activities '
        'enjoyed here include rowing, and riding the summer toboggan run.',
        softWrap: true,
      ),
    );

    loginButon(BuildContext context) => Material(
          elevation: 8.0,
          borderRadius: BorderRadius.circular(60.0),
          color: Color(0xff01A0C7),
          child: MaterialButton(
            padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SecondRoute()));
            },
            child: Text(
              "Next",
              textAlign: TextAlign.center,
              style: style.copyWith(
                  color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
        );

    return MaterialApp(
      title: 'Flutter Layout Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter Layout Demo'),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                children: [
                  Image.asset(
                    'assets/lake.jpg',
                    width: 600,
                    height: 240,
                    fit: BoxFit.cover,
                  ),
                  titleSection,
                  buttonSection,
                  textSection,
                ],
              ),
            ),
            Builder(builder: (context) => loginButon(context))
          ],
        ),
      ),
    );
  }
}

class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  bool _isFavorited = true;
  int _favoriteCount = 41;
  TextStyle style = TextStyle(fontSize: 20.0);

  void _toggleFavorite() {
    setState(() {
      if (_isFavorited) {
        _favoriteCount -= 1;
        _isFavorited = false;
      } else {
        _favoriteCount += 1;
        _isFavorited = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(0),
          child: IconButton(
            icon: (_isFavorited ? Icon(Icons.star) : Icon(Icons.star_border)),
            color: Colors.red[500],
            onPressed: _toggleFavorite,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$_favoriteCount'),
          ),
        ),
      ],
    );
  }
}

class SecondRoute extends StatefulWidget {
  final String username;

  const SecondRoute({Key key, this.username}) : super(key: key);

  @override
  _SecondRouteState createState() => _SecondRouteState();

  static const imageUrls = [
    'http://static.asiawebdirect.com/m/kl/portals/langkawi-info-com/homepage/pantai-tengah/pagePropertiesImage/pantai-tengah-langkawi.jpg.jpg',
    'http://static.asiawebdirect.com/m/kl/portals/langkawi-info-com/homepage/pantai-tengah/pagePropertiesImage/pantai-tengah-langkawi.jpg.jpg'
  ];
}

class _SecondRouteState extends State<SecondRoute> {
  bool showGrid = false;
  bool _active = false;

  void _handletap() {
    setState(() {
      _active = !_active;
    });
  }

  Widget build(BuildContext context) {

    Widget hurufSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Snow Aku Lapar',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'Kuala Lumpur',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          Icon(
            Icons.star,
            color: Colors.blue[500],
          ),
          Text('41'),
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Second Page'),
      ),
      body: ListView(
        children: <Widget>[
          GestureDetector(
            child: Hero(
              tag: 'imageHero',
              child: Image.network(
                  'http://static.asiawebdirect.com/m/kl/portals/langkawi-info-com/homepage/pantai-tengah/pagePropertiesImage/pantai-tengah-langkawi.jpg.jpg'),
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) {
                return DetailScreen();
              }));
            },
          ),
          hurufSection
        ],
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget hurufSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Snow Aku Lapar',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'Kuala Lumpur',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          Icon(
            Icons.star,
            color: Colors.blue[500],
          ),
          Text('41'),
        ],
      ),
    );

    Widget wordSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'He gives his harness bells a shake. To ask if there is some mistake. The only other sound’s the sweep Of easy wind and downy flake.The woods are lovely, dark and deep',
        softWrap: true,
      ),
    );
    return Scaffold(
      body: ListView(
        children: [
          GestureDetector(
            child: Image.network(
              'http://static.asiawebdirect.com/m/kl/portals/langkawi-info-com/homepage/pantai-tengah/pagePropertiesImage/pantai-tengah-langkawi.jpg.jpg',
              width: 600,
              height: 240,
              fit: BoxFit.cover,
            ),
            onTap: Navigator.of(context).pop,
          ),
          wordSection,
          hurufSection,
        ],
      ),
    );
  }
}
